SELECT department.name_department,employee.surname,employee.name,employee.middle_name,
employee.working_time FROM department INNER JOIN employee ON (department.id_department = employee.id_department);


SELECT clients_data.surname,clients_data.name,clients_data.middle_name,
account.number_account,deposit.create_data,deposit.suma,credit.take_data,credit.sum_take,
credit.sum_return,storehouse.create_date
FROM clients_data 
LEFT OUTER JOIN account ON (account.id_client=clients_data.id_client)
LEFT OUTER JOIN deposit ON (account.id_account=deposit.id_account)
LEFT OUTER JOIN credit ON(account.id_account=credit.id_account)
LEFT OUTER JOIN storehouse ON(clients_data.id_client=storehouse.id_client)
ORDER BY clients_data.surname ASC;


SELECT currency.currency_name, 
	SUM(banknote.nominal_currency*banknote.number_banknote) AS sum_mon 
FROM currency 
LEFT OUTER JOIN banknote ON(currency.id_currency=banknote.id_currency)
GROUP BY banknote.id_currency,currency.currency_name
ORDER BY sum_mon DESC;


SELECT clients_data.id_client,clients_data.surname,clients_data.name,
clients_data.middle_name,deposit.suma
FROM clients_data INNER JOIN deposit USING(id_client)
UNION
SELECT clients_data.id_client,clients_data.surname,clients_data.name,
clients_data.middle_name,credit.sum_take
FROM clients_data INNER JOIN credit USING(id_client);
