INSERT INTO banks(address,phone) VALUES ('Kyiv Metalistiv 3','33-44-55'),('Kyiv Geroiv Sevastopolia 7','+380985556464');
INSERT INTO department(name_department,id_bank) VALUES ('Credituvania',1),('Bisnes',1);
INSERT INTO employee(surname,name,middle_name,position,date_bith,work_begin,working_time,passport_series,passport_number,id_department) VALUES ('Petrov','Ivan','Sergiyovich','kasir','1992-03-30','2012-03-30','(08:00,13:00)','AU','123456',1),('Stepanov','Roman','Sergiyovich','st.meneger','1982-03-30','2011-03-30','(07:00,16:00)','AU','123446',2);
INSERT INTO currency(currency_name,sum_currency) VALUES ('EUR','100000'),('UAH','200000');
INSERT INTO banknote(id_currency,nominal_currency,number_banknote) VALUES (1,100,100),(1,50,1000),(1,20,1000),(2,100,100),(2,50,1000),(2,20,1000),(2,200,6000);
INSERT INTO clients_data(surname,name,middle_name,phone,passport_series,passport_number,id_number) VALUES ('Ivanov','Ivan','Sergiyovich','+380963214585','AU','111456','1234567890'),('Stepashka','Ivan','Ivanovich','+380963334585','AU','123226','1234561190'),
('Piatochkin','Petro','Igorovich','+380963244585','AU','123156','1234007890');
INSERT INTO account(number_account,date_open,date_close,id_currency,suma,id_client) VALUES ('12345678909876543666','2013-03-10','2015-03-10',2,100000,1),('12345624909876543212','2013-03-13','2015-03-13',1,10000,2);
INSERT INTO deposit(id_account,create_data,suma,id_currency,percent,close_date,id_client) VALUES (1,'2013-03-10',100000,2,10,'2014-03-10',1);
INSERT INTO credit(id_account,take_data,sum_take,sum_return,id_currency,percent,date_return_credit,id_client) VALUES (2,'2013-03-12',10000,0,1,10,'2014-03-12',2);
INSERT INTO storehouse(id_client,create_date,close_date,id_bank) VALUES (3,'2013-01-22','2015-01-21',1);


