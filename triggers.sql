CREATE OR REPLACE FUNCTION trig_date_funk() RETURNS trigger AS $trig_date$
 DECLARE
 BEGIN
  IF (current_date-NEW.date_bith) < 6575 THEN 
  	RAISE EXCEPTION 'very young employee';
  END IF;
  IF (SELECT id_department FROM department WHERE id_department = NEW.id_department) IS NULL THEN
 	RAISE EXCEPTION 'this foreign key id_department does not exist';
  END IF;
  RETURN NEW;
 END;
$trig_date$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_date BEFORE INSERT OR UPDATE ON employee FOR each ROW EXECUTE PROCEDURE trig_date_funk();



CREATE OR REPLACE FUNCTION trig_summa_funk() RETURNS trigger AS $trig_sum_currency$
 DECLARE
   all_sum numeric(10,2);
 BEGIN
   SELECT SUM(nominal_currency*number_banknote) INTO all_sum FROM banknote WHERE id_currency = NEW.id_currency;
   UPDATE currency SET sum_currency = all_sum WHERE id_currency = NEW.id_currency;
   RETURN NEW;
 END;
$trig_sum_currency$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_sum_currency AFTER INSERT OR UPDATE ON banknote FOR each ROW EXECUTE PROCEDURE trig_summa_funk();


CREATE TABLE deposit_reg(
	user_name 		varchar NOT NULL,
	date_edit		timestamptz NOT NULL,
	operation 		varchar NOT NULL,											
	id_deposit		int NOT NULL,
	id_account		int NOT NULL,		
	create_data		date NOT NULL,			
	suma			int NOT NULL,		
	id_currency		int NOT NULL,	
	percent			float NOT NULL,		
	close_date		date NOT NULL,                      
        id_client		int NOT NULL
);

CREATE OR REPLACE FUNCTION trig_deposit_reg_funk() RETURNS trigger AS $trig_deposit_reg$
 DECLARE
 BEGIN
   IF (TG_OP = 'DELETE') THEN
 	  INSERT INTO deposit_reg SELECT user, now(), TG_OP, OLD.id_deposit, OLD.id_account, 
          OLD.create_data, OLD.suma, OLD.id_currency,OLD.percent,OLD.close_date,OLD.id_client;
          RETURN OLD;
   ELSIF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
 	  INSERT INTO deposit_reg SELECT user, now(), TG_OP, NEW.id_deposit, NEW.id_account, 
          NEW.create_data, NEW.suma, NEW.id_currency,NEW.percent,NEW.close_date,NEW.id_client;
          RETURN NEW;
   END IF;
 END;
$trig_deposit_reg$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_deposit_reg AFTER INSERT OR UPDATE OR DELETE ON deposit FOR each ROW EXECUTE PROCEDURE trig_deposit_reg_funk();




