CREATE TYPE w_time AS (t1 time, t2 time);
CREATE TABLE banks(
	id_bank			serial PRIMARY KEY,      --1.1
	address			varchar(35) NOT NULL, --1.1
	phone			varchar(13) NOT NULL --1.1
);
CREATE TABLE department(
	id_department		serial PRIMARY KEY,		--1.1
	name_department		varchar(20) NOT NULL UNIQUE,	--1.1
	id_bank			int REFERENCES banks		--1.3
);
CREATE TABLE employee(
	id_employee		serial PRIMARY KEY,		--1.1
	surname			varchar(30) NOT NULL,		--1.1
	name			varchar(30) NOT NULL,		--1.1
	middle_name		varchar(30) NOT NULL,		--1.1
	position		varchar(25) NOT NULL,		--1.1
	date_bith		date CHECK (current_date-date_bith>=6575), --1.1
	work_begin		date NOT NULL,			--1.1
	working_time		w_time NOT NULL, 		--1.1
	passport_series		char(2) NOT NULL,		--1.1
	passport_number		char(6) NOT NULL,		--1.1
	id_department		int REFERENCES department,	--1.3
	UNIQUE(passport_series,passport_number)			--1.1
);
CREATE TABLE currency(
	id_currency		serial PRIMARY KEY,		--1.1
	currency_name		varchar(25) NOT NULL UNIQUE,	--1.1
	sum_currency		numeric(10,2) NOT NULL		--1.1
);
CREATE TABLE banknote(
	id_currency		int REFERENCES currency,	--1.3
	nominal_currency	int NOT NULL,			--1.1
	number_banknote		int NOT NULL			--1.1
);
CREATE TABLE clients_data(
	id_client		serial PRIMARY KEY,		--1.1
	surname			varchar(30) NOT NULL,		--1.1
	name			varchar(30) NOT NULL,		--1.1
	middle_name		varchar(30) NOT NULL,		--1.1
	phone			varchar(13) NOT NULL,		--1.1
	passport_series		char(2) NOT NULL,		--1.1
	passport_number		char(6) NOT NULL,		--1.1
	id_number		char(10) NOT NULL,		--1.1
	UNIQUE(passport_series,passport_number)			--1.1
);
CREATE TABLE account(
	id_account		serial PRIMARY KEY,		--1.1
	number_account		varchar(20) NOT NULL UNIQUE,	--1.1
	date_open		date NOT NULL,			--1.1
	date_close		date CHECK(date_close>date_open), --1.2
	id_currency		int REFERENCES currency,	 --1.3
	suma			int NOT NULL,			--1.1
	id_client		int REFERENCES clients_data	--1.3
);
CREATE TABLE deposit(
	id_deposit		serial PRIMARY KEY,		--1.1
	id_account		int REFERENCES account,		--1.3
	create_data		date NOT NULL,			--1.1
	suma			int CHECK(suma>0),		--1.1
	id_currency		int REFERENCES currency,	--1.3	
	percent			float CHECK(percent>0),		--1.1
	close_date		date CHECK(close_date>create_data), --1.2
	id_client		int REFERENCES clients_data	 --1.3
);
CREATE TABLE credit(
	id_credit		serial PRIMARY KEY,		--1.1
	id_account		int REFERENCES account,		--1.3
	take_data		date NOT NULL,			--1.1
	sum_take		int CHECK(sum_take>0),		--1.1
	sum_return		int CHECK(sum_return>=0),	--1.1
	id_currency		int REFERENCES currency,	--1.3
	percent			float CHECK(percent>0),		--1.1
	date_return_credit	date CHECK(date_return_credit>take_data),	--1.2
	id_client		int REFERENCES clients_data		--1.3
);
CREATE TABLE storehouse(
	id_box			serial PRIMARY KEY,		--1.1
	id_client		int REFERENCES clients_data,	--1.3
	create_date		date NOT NULL,			--1.1
	close_date		date CHECK(close_date>create_date), --1.2
	id_bank			int REFERENCES banks		--1.3
);
