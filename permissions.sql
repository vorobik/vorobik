CREATE ROLE dbadmin WITH SUPERUSER CREATEROLE CREATEDB LOGIN ENCRYPTED PASSWORD '1';
CREATE ROLE admin WITH CREATEROLE LOGIN ENCRYPTED PASSWORD '1';
CREATE ROLE dbuser WITH LOGIN ENCRYPTED PASSWORD '1';
GRANT CONNECT ON DATABASE bank_database TO dbuser;
GRANT CONNECT ON DATABASE bank_database TO admin;
GRANT SELECT, INSERT, UPDATE ON ALL TABLES IN SCHEMA public TO admin;
GRANT SELECT(id_bank, address, phone) ON banks TO dbuser; 
GRANT SELECT(id_department, name_department, id_bank) ON department TO dbuser; 
GRANT SELECT(id_employee, surname,name,middle_name,position,work_begin,working_time,id_department) ON employee TO dbuser; 
GRANT SELECT(id_currency,currency_name,sum_currency) ON currency TO dbuser;  
GRANT SELECT(id_currency,nominal_currency,number_banknote) ON banknote TO dbuser;
GRANT SELECT(id_client,surname,name,middle_name,phone) ON clients_data TO dbuser;
GRANT SELECT(id_account,number_account,id_client) ON account TO dbuser;
GRANT SELECT(id_deposit,id_account,create_data,suma,id_currency,percent,close_date,id_client) ON deposit TO dbuser;
GRANT SELECT(id_credit,id_account,take_data,sum_take,sum_return,id_currency,percent,date_return_credit,id_client) ON credit TO dbuser;
GRANT SELECT(id_box,id_client,create_date,close_date,id_bank) ON storehouse TO dbuser;

